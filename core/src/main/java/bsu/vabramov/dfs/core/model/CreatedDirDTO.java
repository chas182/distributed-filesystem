package bsu.vabramov.dfs.core.model;

/**
 * Created by vlad on 18.1.16.
 */
public class CreatedDirDTO {

    public boolean created = true;

    public boolean isCreated() {
        return created;
    }

    public boolean getCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }
}
