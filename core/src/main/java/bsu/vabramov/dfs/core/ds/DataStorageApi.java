package bsu.vabramov.dfs.core.ds;

import bsu.vabramov.dfs.core.model.CreatedDirDTO;
import bsu.vabramov.dfs.core.model.Status;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

import java.util.List;


/**
 * @author Vlad
 * @since 17.01.2016
 */
public interface DataStorageApi {

    @GET("/status")
    Call<Status> getStatus();

    @PUT("/create-dir")
    Call<CreatedDirDTO> createDir(@Query("ul") String userLogin, @Query("d") String newDir);

    @DELETE("/delete-file")
    Call<CreatedDirDTO> deleteFile(@Query("ul") String userLogin, @Query("bp") List<String> blocksPaths);
}
