package bsu.vabramov.dfs.core.settings;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author Vlad
 * @since 12.01.2016
 */
@XmlRootElement(name = "settings")
@XmlAccessorType(XmlAccessType.NONE)
public class FSSettings {
    @XmlElement(name = "file-block-size")
    private Integer fileBlockSize;
    @XmlElement(name = "replication-factor")
    private Integer replicationFactor;
    @XmlElement(name = "name-storage")
    private NameStorage nameStorage;

    @XmlElements({
            @XmlElement(name = "data-storage", type = DataStorage.class)
    })
    @XmlElementWrapper(name = "data-storages")
    private List<DataStorage> dataStorages;

    public List<DataStorage> getDataStorages() {
        return dataStorages;
    }

    public DataStorage getDataStorage(int id) {
        return dataStorages.stream().filter(ds -> ds.getId() == id).findAny().get();
    }

    public Integer getFileBlockSize() {
        return fileBlockSize;
    }

    public Integer getReplicationFactor() {
        return replicationFactor;
    }

    public NameStorage getNameStorage() {
        return nameStorage;
    }

    @XmlRootElement(name = "data-storage")
    public static class DataStorage {
        @XmlElement(name = "id")
        private Integer id;
        @XmlElement(name = "data-dir")
        private String dataDir;
        @XmlElement(name = "host")
        private String host;
        @XmlElement(name = "port")
        private Integer port;

        public Integer getId() {
            return id;
        }

        public String getDataDir() {
            return dataDir;
        }

        public String getHost() {
            return host;
        }

        public Integer getPort() {
            return port;
        }
    }

    public static class NameStorage {
        @XmlElement(name = "host")
        private String host;
        @XmlElement(name = "port")
        private Integer port;

        public String getHost() {
            return host;
        }

        public Integer getPort() {
            return port;
        }
    }
}
