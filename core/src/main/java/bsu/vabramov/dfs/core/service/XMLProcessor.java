package bsu.vabramov.dfs.core.service;

import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class XMLProcessor {
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    public void setMarshaller(Marshaller marshaller) {
        this.marshaller = marshaller;
    }

    public void setUnmarshaller(Unmarshaller unmarshaller) {
        this.unmarshaller = unmarshaller;
    }

    public void objectToXML(String fileName, Object graph) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            marshaller.marshal(graph, new StreamResult(fos));
        }
    }

    public Object xmlToObject(File file) throws IOException {
        try (FileInputStream fis = new FileInputStream(file)) {
            return unmarshaller.unmarshal(new StreamSource(fis));
        }
    }

    public Object xmlToObject(InputStream is) throws IOException {
        return unmarshaller.unmarshal(new StreamSource(is));
    }
} 