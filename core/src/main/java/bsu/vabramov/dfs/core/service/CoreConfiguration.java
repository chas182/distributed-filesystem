package bsu.vabramov.dfs.core.service;

import bsu.vabramov.dfs.core.settings.FSSettings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vlad
 * @since 17.01.2016
 */
@Configuration
public class CoreConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(CoreConfiguration.class);
    private static final String SETTINGS_FILE_NAME = "dfs-settings.xml";

    @Value("#{environment.DFS_HOME}")
    private String dfsHomeDir;

    @Bean
    @Scope("singleton")
    @DependsOn("xmlProcessor")
    public FSSettings fsSettings(XMLProcessor xmlProcessor) throws IOException {
//        if (dfsHomeDir == null || "".equals(dfsHomeDir))
//            throw new IllegalArgumentException("DFS_HOME is not set.");
        FSSettings FSSettings;
        try {
            FSSettings = (FSSettings) xmlProcessor.xmlToObject(new ClassPathResource(SETTINGS_FILE_NAME).getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
//            logger.error("{} file not found.", SETTINGS_FILE_NAME);
            throw new FileNotFoundException(SETTINGS_FILE_NAME + " file not found.");
        }
        logger.info("Name storage settings initiated.");
        return FSSettings;
    }

    @Bean
    public XMLProcessor xmlProcessor() {
        XMLProcessor handler = new XMLProcessor();
        handler.setMarshaller(jaxb2Marshaller());
        handler.setUnmarshaller(jaxb2Marshaller());
        return handler;
    }

    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setPackagesToScan("bsu.vabramov.dfs");
        Map<String, Object> map = new HashMap<>();
        map.put("jaxb.formatted.output", true);
        jaxb2Marshaller.setMarshallerProperties(map);
        return jaxb2Marshaller;
    }
}
