package bsu.vabramov.dfs.core.util;

import org.apache.commons.lang.RandomStringUtils;

import java.util.Random;

/**
 * @author Vlad
 * @since 29.05.2016
 */
public final class Utils {

    private static final char[] RANDOM_SOUCE = "-ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
    private static final Random RANDOM = new Random(System.nanoTime());

    public static String randomString(int count) {
        return RandomStringUtils.random(count, 0, RANDOM_SOUCE.length, false, false, RANDOM_SOUCE, RANDOM);
    }
}
