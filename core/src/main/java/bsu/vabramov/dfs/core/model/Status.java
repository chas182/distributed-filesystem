package bsu.vabramov.dfs.core.model;

/**
 * @author Vlad
 * @since 17.01.2016
 */
public class Status {
    private String dataDirectory;

    public String getDataDirectory() {
        return dataDirectory;
    }

    public void setDataDirectory(String dataDirectory) {
        this.dataDirectory = dataDirectory;
    }
}
