package bsu.vabramov.dfs.core.util;

/**
 * @author Vlad Abramov <vabramov@exadel.com>
 * @since 22.1.16
 */
/**
 vabramov@vabramov-H97M-D3H:~$ /bin/bash /home/vabramov/.dfs/dfs-help.sh add-group developers
 vabramov@vabramov-H97M-D3H:~$ /bin/bash /home/vabramov/.dfs/dfs-help.sh add-user devl developers
 vabramov@vabramov-H97M-D3H:~$ /bin/bash /home/vabramov/.dfs/dfs-help.sh remove-group developers
 vabramov@vabramov-H97M-D3H:~$ /bin/bash /home/vabramov/.dfs/dfs-help.sh remove-user devl
 * */
public enum DfsHelperOperations {
    ADD_USER("dfs-help.sh add-group developers");


    private final String command;

    DfsHelperOperations(String command) {
        this.command = command;
    }
}
