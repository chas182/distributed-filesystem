package bsu.vabramov.dfs.namestorage.web.client;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import bsu.vabramov.dfs.namestorage.annotation.AuthUser;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFile;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFileHistoryItem;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSOperationDTO;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileStatuses;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileTree;
import bsu.vabramov.dfs.namestorage.model.pojo.user.User;
import bsu.vabramov.dfs.namestorage.service.NameStorageService;

/**
 * @author Vlad
 * @since 18.01.2016
 */
@RestController
@PropertySource("classpath:settings.properties")
public class BrowserController extends ClientController {

    @Value("${data.directory}")
    private String dataDirectory;
    @Autowired
    private NameStorageService fileStorageService;

    @RequestMapping(value = "/files-info", method = RequestMethod.GET)
    public FileTree getFilesInfo(@AuthUser User user) {
        return fileStorageService.getFilesInfo(user);
    }

    @RequestMapping(value = "/fs", params = {"op=LS"}, method = RequestMethod.GET)
    public FileStatuses listFiles(@RequestParam String du, @AuthUser User user) {
        return fileStorageService.getFileStatuses(du, user);
    }
    @RequestMapping(value = "/fs", params = {"op=INF"}, method = RequestMethod.GET)
    public FSFile fileInfo(@RequestParam String du, @AuthUser User user, HttpServletRequest request) {
        return fileStorageService.getFileInfo(du, user);
    }

    @RequestMapping(value = "/fs", params = {"op=CREATE_DIR"},  method = RequestMethod.PUT)
    public FSOperationDTO createDir(@RequestParam String pid, @RequestParam String nd, @AuthUser User user) {
        return fileStorageService.createDir(pid, nd, user);
    }

    @RequestMapping(value = "/fs/upload", method = RequestMethod.POST)
    public FSOperationDTO uploadPOST(@RequestParam MultipartFile file, @RequestParam String pid, @AuthUser User user) {
        return fileStorageService.createFile(user, pid, file);
    }

    @RequestMapping(value = "/download/{fileUid}", method = RequestMethod.GET)
    public void downloadFile(HttpServletResponse response, @PathVariable String fileUid, @AuthUser User user) {
        fileStorageService.sendFile(user, fileUid, response);
    }

    @RequestMapping(value = "/history/{fileUid}", method = RequestMethod.GET)
    public List<FSFileHistoryItem> getHistory(HttpServletResponse response, @PathVariable String fileUid, @AuthUser User user) {
        return fileStorageService.getHistory(user, fileUid);
    }
}
