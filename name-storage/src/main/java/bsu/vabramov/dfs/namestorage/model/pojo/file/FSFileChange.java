package bsu.vabramov.dfs.namestorage.model.pojo.file;

/**
 * @author Vlad
 * @since 06.06.2016
 */

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//@Entity
public class FSFileChange {
    @Id
    @GeneratedValue
    private Long id;
    private String fileUid;
}
