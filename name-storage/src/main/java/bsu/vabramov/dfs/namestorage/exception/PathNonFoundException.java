package bsu.vabramov.dfs.namestorage.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Vlad
 * @since 18.01.2016
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PathNonFoundException extends RuntimeException {
}
