package bsu.vabramov.dfs.namestorage.model.pojo.file;

import java.util.List;

/**
 * @author Vlad
 * @since 18.01.2016
 */
public class FileStatuses {

    private List<FSFile> files;

    public FileStatuses() {
    }

    public FileStatuses(List<FSFile> files) {
        this.files = files;
    }

    public List<FSFile> getFiles() {
        return files;
    }

    public void setFiles(List<FSFile> files) {
        this.files = files;
    }

}
