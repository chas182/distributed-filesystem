package bsu.vabramov.dfs.namestorage.model.dao;

import bsu.vabramov.dfs.namestorage.model.pojo.file.FileBlock;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Vlad Abramov <vabramov@exadel.com>
 * @since 15.2.16
 */
@Repository
public interface FileBlockRepository extends CrudRepository<FileBlock, Long> {

    List<FileBlock> findAllByFileUid(String uid);
}
