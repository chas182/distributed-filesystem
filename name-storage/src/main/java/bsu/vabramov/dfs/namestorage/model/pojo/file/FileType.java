package bsu.vabramov.dfs.namestorage.model.pojo.file;

/**
 * @author Vlad
 * @since 18.01.2016
 */
public enum FileType {
    FILE, DIRECTORY
}
