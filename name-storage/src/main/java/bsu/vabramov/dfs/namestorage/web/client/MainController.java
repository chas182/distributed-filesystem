package bsu.vabramov.dfs.namestorage.web.client;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Vlad
 * @since 10.01.2016
 */
@Controller
public class MainController extends ClientController {


    @RequestMapping(value = "", method = RequestMethod.GET)
    public String greeting(Model model) {
        return "index";
    }
}
