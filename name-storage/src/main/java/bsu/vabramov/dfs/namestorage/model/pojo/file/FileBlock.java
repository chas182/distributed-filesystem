package bsu.vabramov.dfs.namestorage.model.pojo.file;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Vlad
 * @since 31.05.2016
 */
@Entity
public class FileBlock {

    @Id
    @GeneratedValue
    private Long id;
    private String dsUrls;
    private String fileUid;
    private String blockPath;
    private int indexNumber;
    private long blockSize;

    public FileBlock() {
	}

	public FileBlock(String fileUid, String blockPath, int indexNumber, String dataStorageUrl) {
		this.fileUid = fileUid;
		this.blockPath = blockPath;
		this.indexNumber = indexNumber;
		this.dsUrls = dataStorageUrl;
	}

	public void addDataStorageUrl(String url) {
		dsUrls += ";" + url;
	}
	
	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<String> getDataStorageUlrs() {
		return Arrays.asList(dsUrls.split(";"));
	}
    
    public String getDsUrls() {
		return dsUrls;
	}

	public void setDsUrls(String dsUrls) {
		this.dsUrls = dsUrls;
	}

	public String getFileUid() {
        return fileUid;
    }

    public void setFileUid(String fileUid) {
        this.fileUid = fileUid;
    }

    public String getBlockPath() {
		return blockPath;
	}

	public void setBlockPath(String blockPath) {
		this.blockPath = blockPath;
	}

	public int getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(int indexNumber) {
        this.indexNumber = indexNumber;
    }

    public long getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(long blockSize) {
		this.blockSize = blockSize;
	}

	public static final Comparator<FileBlock> INDEX_NUMBER_COMPARATOR = (o1, o2) -> o1.indexNumber - o2.indexNumber;
}
