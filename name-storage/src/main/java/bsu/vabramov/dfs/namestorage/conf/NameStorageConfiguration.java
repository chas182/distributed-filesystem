package bsu.vabramov.dfs.namestorage.conf;

import bsu.vabramov.dfs.core.ds.DataStorageApi;
import bsu.vabramov.dfs.core.service.CoreConfiguration;
import bsu.vabramov.dfs.core.settings.FSSettings;
import bsu.vabramov.dfs.namestorage.service.DataStorageService;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

import javax.annotation.PreDestroy;
import javax.servlet.MultipartConfigElement;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.UUID;

/**
 * @author Vlad
 * @since 12.01.2016
 */
@Configuration
@Import(CoreConfiguration.class)
public class NameStorageConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(NameStorageConfiguration.class);

    @Autowired
    private Path tempDir;

    @Bean
    @DependsOn("fsSettings")
    public DataStorageService dataStorageService(FSSettings FSSettings) {
        DataStorageService service = new DataStorageService();
        FSSettings.getDataStorages().forEach(dataStorage -> {
            String dsUrl = "http://" + dataStorage.getHost() + ":" + dataStorage.getPort();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(dsUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service.addDataStorageServer(dsUrl, retrofit.create(DataStorageApi.class));
        });
        return service;
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("10240MB");
        factory.setMaxRequestSize("10240MB");
        return factory.createMultipartConfig();
    }

//    @Bean
//    public MultipartResolver multipartResolver() {
//        org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
//        multipartResolver.setMaxUploadSize(500 * 1024 * 1024);
//        return multipartResolver;
//    }

    @Bean
    public ShaPasswordEncoder passwordEncoder() {
        return new ShaPasswordEncoder(256);
    }
    
    @Bean
    public HttpClient httpClient() {
        return HttpClientBuilder.create().build();
    }

    @Bean
    public Path tempDir() throws Exception {
        return Files.createTempDirectory("nstemp-" + UUID.randomUUID().toString());
    }

    @PreDestroy
    public void clearTmpDir() throws Exception {
        Files.walkFileTree(tempDir, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
}
