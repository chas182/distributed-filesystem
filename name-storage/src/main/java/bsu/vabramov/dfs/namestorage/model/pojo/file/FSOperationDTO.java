package bsu.vabramov.dfs.namestorage.model.pojo.file;

import java.util.HashMap;
import java.util.Map;

public class FSOperationDTO {

	public static final Integer STATUS_OPERATION_FAILED = -1;
	public static final Integer STATUS_SUCCESS = 1;
	public static final Integer STATUS_DIRECTORY_EXISTS = 2;
	public static final Integer STATUS_FILE_EXISTS = 3;
	public static final Integer STATUS_SUCCESS_UPDATE = 4;

	private FileTree fileTree;
	private int statusCode = STATUS_SUCCESS;
	private Map<String, Object> info = new HashMap<>();
	
	public FileTree getFileTree() {
		return fileTree;
	}
	public void setFileTree(FileTree fileTree) {
		this.fileTree = fileTree;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public void putInfo(String key, Object val) {
		info.put(key, val);
	}
	public Map<String, Object> getInfo() {
		return info;
	}
	
}
