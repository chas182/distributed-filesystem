package bsu.vabramov.dfs.namestorage.service;

import bsu.vabramov.dfs.core.util.Utils;
import bsu.vabramov.dfs.namestorage.model.dao.FileRepository;
import bsu.vabramov.dfs.namestorage.model.dao.UserRepository;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFile;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileType;
import bsu.vabramov.dfs.namestorage.model.pojo.user.User;
import bsu.vabramov.dfs.namestorage.model.pojo.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by vlad on 14.2.16.
 */
@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private ShaPasswordEncoder passwordEncoder;
    @Autowired
    private DataStorageService dataStorageService;

    public User createNewUser(String login, String password) {
        User newUser = new User();
        newUser.setUserRole(UserRole.ROLE_USER);
        newUser.setLogin(login);
        newUser.setUid(Utils.randomString(13));
        newUser.setPassword(passwordEncoder.encodePassword(password, login));
        userRepository.save(newUser);

        FSFile rootFolder = new FSFile();
        rootFolder.setFileType(FileType.DIRECTORY);
        rootFolder.setName("User disk");
        rootFolder.setPath("/");
        rootFolder.setOwnerUid(newUser.getUid());
        rootFolder.setUid(newUser.getUid() + Utils.randomString(13));
        rootFolder.setParentUid("");
        rootFolder.setOwnerName(newUser.getLogin());

        fileRepository.save(rootFolder);
        return newUser;
    }

    void createAdmin(String login, String password) {
        User newUser = new User();
        newUser.setUserRole(UserRole.ROLE_ADMIN);
        newUser.setLogin(login);
        newUser.setPassword(passwordEncoder.encodePassword(password, login));
        newUser.setUid(Utils.randomString(13));
        userRepository.save(newUser);
    }

}
