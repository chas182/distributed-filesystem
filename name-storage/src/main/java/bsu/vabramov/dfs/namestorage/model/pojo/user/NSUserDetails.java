package bsu.vabramov.dfs.namestorage.model.pojo.user;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class NSUserDetails extends org.springframework.security.core.userdetails.User {

    private User user;

    public NSUserDetails(User user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getLogin(), user.getPassword(), true, true, true, true, authorities);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}