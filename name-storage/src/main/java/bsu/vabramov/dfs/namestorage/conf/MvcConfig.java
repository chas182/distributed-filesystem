package bsu.vabramov.dfs.namestorage.conf;

import bsu.vabramov.dfs.namestorage.util.AuthUserMethodArgumentResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/need-access").setViewName("need-access");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(authUserMethodArgumentResolver());
    }

    @Bean
    public HandlerMethodArgumentResolver authUserMethodArgumentResolver() {
        return new AuthUserMethodArgumentResolver();
    }
}