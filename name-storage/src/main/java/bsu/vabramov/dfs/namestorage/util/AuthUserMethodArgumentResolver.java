package bsu.vabramov.dfs.namestorage.util;

import bsu.vabramov.dfs.namestorage.annotation.AuthUser;
import bsu.vabramov.dfs.namestorage.model.pojo.user.NSUserDetails;
import bsu.vabramov.dfs.namestorage.model.pojo.user.User;
import org.springframework.core.MethodParameter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.lang.annotation.Annotation;
import java.security.Principal;

/**
 * @author vabramov
 */
public class AuthUserMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return supportsAnnotation(methodParameter, AuthUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer modelAndViewContainer,
                                  NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory) throws Exception {
        if (this.supportsParameter(methodParameter)) {
            if (methodParameter.getParameterAnnotation(AuthUser.class) != null) {
                return resolveActiveUserArgument(methodParameter, nativeWebRequest);
            }
        }
        return WebArgumentResolver.UNRESOLVED;
    }

    private Object resolveActiveUserArgument(MethodParameter methodParameter, NativeWebRequest nativeWebRequest) {
        NSUserDetails userDetails = getUserDetails(nativeWebRequest);
        if (userDetails == null)
            return WebArgumentResolver.UNRESOLVED;
        return userDetails.getUser();
    }

    private <T extends Annotation> boolean supportsAnnotation(MethodParameter methodParameter, Class<T> annotationClass) {
        return methodParameter.getParameterAnnotation(annotationClass) != null
                && methodParameter.getParameterType().equals(User.class);
    }

    private NSUserDetails getUserDetails(NativeWebRequest nativeWebRequest) {
        Principal principal = nativeWebRequest.getUserPrincipal();
        if (principal == null)
            throw new AccessDeniedException("Access is denied");
        return (NSUserDetails) ((Authentication) principal).getPrincipal();
    }
}
