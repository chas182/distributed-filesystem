package bsu.vabramov.dfs.namestorage.model.pojo.file;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.joda.time.DateTime;

import bsu.vabramov.dfs.core.util.Utils;
import bsu.vabramov.dfs.namestorage.model.pojo.user.User;

/**
 * @author Vlad Abramov <vabramov@exadel.com>
 * @since 15.2.16
 */
@Entity
public class FSFile implements Comparable<FSFile> {
    @Id
    @GeneratedValue
    private Long id;
    private String uid;
    @Enumerated(value = EnumType.STRING)
    private FileType fileType = FileType.DIRECTORY;
    private String name;
    private String path;
    private String ownerUid;
    private String ownerName;
    private long length;
    private long lastModified = DateTime.now().getMillis();
    private long created = DateTime.now().getMillis();
    private String parentUid;

    public FSFile() {
    }

    public FSFile(User owner, FSFile parent, String name) {
        this.name = name;
        this.ownerName = owner.getLogin();
        this.ownerUid = owner.getUid();
        this.uid = this.ownerUid + Utils.randomString(13);
        this.parentUid = parent.getUid();
        this.path = parent.getPath() +
                (parent.getPath().charAt(parent.getPath().length() - 1) == '/' ? "" : "/") +
                name;
    }

    public FSFile(User owner, FSFile parent, String name, FileType fileType) {
        this(owner, parent, name);
        this.fileType = fileType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerUid() {
        return ownerUid;
    }

    public void setOwnerUid(String ownerUid) {
        this.ownerUid = ownerUid;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getParentUid() {
        return parentUid;
    }

    public void setParentUid(String parentUid) {
        this.parentUid = parentUid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FSFile fsFile = (FSFile) o;

        return id != null ? id.equals(fsFile.id) : fsFile.id == null && uid.equals(fsFile.uid);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + uid.hashCode();
        return result;
    }

    @Override
    public int compareTo(FSFile other) {
        int cmp = 0;

        if (!fileType.equals(other.fileType)) {
            cmp = fileType == FileType.DIRECTORY && other.fileType == FileType.FILE ? --cmp : ++cmp;
        } else {
            cmp = name.compareTo(other.getName());
        }

        return cmp;
    }
}
