package bsu.vabramov.dfs.namestorage.service;

import bsu.vabramov.dfs.namestorage.model.dao.UserRepository;
import bsu.vabramov.dfs.namestorage.model.pojo.user.NSUserDetails;
import bsu.vabramov.dfs.namestorage.model.pojo.user.User;
import bsu.vabramov.dfs.namestorage.model.pojo.user.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private UserRepository userRepository;
    private UserService userService;
    private static boolean superAdminSet = false;

    @Autowired
    public UserDetailsService(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (!superAdminSet) {
            addSuperAdmin();
        }
        User domainUser = userRepository.findByLogin(username);

        if (domainUser == null) {
            throw new UsernameNotFoundException(username);
        }

        return new NSUserDetails(domainUser, getAuthorities(domainUser.getUserRole()));
    }

    public Collection<? extends GrantedAuthority> getAuthorities(UserRole role) {
        List<GrantedAuthority> authList = new ArrayList<>();
        switch (role) {
            case ROLE_ADMIN:
                authList.add(new SimpleGrantedAuthority(UserRole.ROLE_ADMIN.toString()));
                break;
            default:
                authList.add(new SimpleGrantedAuthority(UserRole.ROLE_USER.toString()));
        }
        return authList;
    }

    private void addSuperAdmin() {
        String login = "admin";
        if (userRepository.findByLogin(login) == null) {
            userService.createAdmin(login, "admin");
        }
        superAdminSet = true;
    }

}