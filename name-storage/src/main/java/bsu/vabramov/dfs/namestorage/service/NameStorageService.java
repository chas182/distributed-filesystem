package bsu.vabramov.dfs.namestorage.service;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import bsu.vabramov.dfs.core.settings.FSSettings;
import bsu.vabramov.dfs.core.util.Utils;
import bsu.vabramov.dfs.namestorage.exception.PathNonFoundException;
import bsu.vabramov.dfs.namestorage.model.dao.FileBlockRepository;
import bsu.vabramov.dfs.namestorage.model.dao.FileRepository;
import bsu.vabramov.dfs.namestorage.model.dao.HistoryItemRepository;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFile;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFileHistoryItem;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSOperationDTO;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileBlock;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileStatuses;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileTree;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileType;
import bsu.vabramov.dfs.namestorage.model.pojo.user.User;

/**
 * @author Vlad
 * @since 18.01.2016
 */
@Service
@PropertySource("classpath:settings.properties")
@Transactional
public class NameStorageService {

	private static final Logger logger = LoggerFactory.getLogger(NameStorageService.class);

	@Autowired
	private FSSettings fsSettings;
	@Autowired
	private DataStorageService dataStorageService;
	@Autowired
	private FileRepository fileRepository;
	@Autowired
	private FileBlockRepository fileBlockRepository;
	@Autowired
	private HistoryItemRepository historyItemRepository;

	public FSFile getFileInfo(String fileUid, User owner) {
		return fileRepository.findByUid(fileUid);
	}

	public FileStatuses getFileStatuses(String rootDirUid, User owner) {
		List<FSFile> fsFiles = fileRepository.findAllByParentUid(rootDirUid);
		Collections.sort(fsFiles);
		return new FileStatuses(fsFiles);
	}

	public FileTree getFilesInfo(User user) {
		List<FSFile> files = fileRepository.findAllByOwnerUid(user.getUid());
		if (files.isEmpty())
			throw new PathNonFoundException();

		FSFile root = getRoot(files);
		FileTree fileTree = new FileTree();
		fileTree.setText(root.getPath());
		fileTree.setType("root");
		fileTree.setId(root.getUid());
		buildFileTree(root, files, fileTree);
		return fileTree;
	}

	private void buildFileTree(FSFile rootDir, List<FSFile> files, FileTree treeNode) {
		List<FSFile> curFiles = getFilesForDir(rootDir, files)
				.stream()
				.filter(f -> f.getFileType() == FileType.DIRECTORY)
				.collect(Collectors.toList());
		Collections.sort(curFiles);
		List<FileTree> children = new LinkedList<>();
		for (FSFile f : curFiles) {
			FileTree ft = new FileTree();
			ft.setText(f.getName());
			ft.setId(f.getUid());
			ft.setType(f.getFileType() == FileType.FILE ? "file" : "dir");
			children.add(ft);
		}
		children.forEach(f -> buildFileTree(getFilesByName(files, f.getText()), files, f));
		treeNode.setChildren(children);
		FileStatuses fs = new FileStatuses();
		fs.setFiles(curFiles);
	}

	private FSFile getRoot(List<FSFile> files) {
		return files.stream().filter(f -> f.getPath().equals("/")).findAny().get();
	}

	private List<FSFile> getFilesForDir(FSFile dir, List<FSFile> files) {
		return files.stream().filter(f -> dir.getUid().equals(f.getParentUid())).collect(Collectors.toList());
	}

	private FSFile getFilesByName(List<FSFile> files, String name) {
		return files.stream().filter(f -> f.getName().equals(name)).findAny().get();
	}

	public FSOperationDTO createDir(String parentUid, String dirName, User user) {
		FSOperationDTO operationDTO = new FSOperationDTO();
		FSFile newDir = checkFileExist(operationDTO, parentUid, dirName);
		if (newDir != null) {
			return operationDTO;
		}
		FSFile parent = fileRepository.findByUid(parentUid);
		newDir = new FSFile(user, parent, dirName);
		String newDirPathStr = newDir.getPath();
		if (dataStorageService.createDir(user.getLogin(), newDirPathStr)) {
			fileRepository.save(newDir);
			parent.setLastModified(DateTime.now().getMillis());
			fileRepository.save(parent);
			logger.info("Created dir: {}", newDir.getName());
			operationDTO.setFileTree(getFilesInfo(user));
		} else {
			logger.error("Failed to create dir: {}", newDirPathStr);
			operationDTO.setStatusCode(FSOperationDTO.STATUS_OPERATION_FAILED);
		}
		return operationDTO;
	}

	public FSOperationDTO createFile(User user, String parentUid, MultipartFile file) {
		FSOperationDTO dto = new FSOperationDTO();

		boolean flag, update = false;
		FSFile parentDir = fileRepository.findByUid(parentUid);
		FSFile fsFile = checkFileExist(dto, parentUid, file.getOriginalFilename());
		if (fsFile != null) {
			if (fsFile.getFileType() == FileType.DIRECTORY) {
				dto.setStatusCode(FSOperationDTO.STATUS_DIRECTORY_EXISTS);
				return dto;
			}
			update = true;
			List<FileBlock> fileBlocks = fileBlockRepository.findAllByFileUid(fsFile.getUid());
			flag = dataStorageService.deleteFileBlocks(user, fileBlocks);
			fileBlockRepository.delete(fileBlocks);
			if (!flag) {
				dto.setStatusCode(FSOperationDTO.STATUS_OPERATION_FAILED);
			}
		} else {
			fsFile = new FSFile(user, parentDir, file.getOriginalFilename(), FileType.FILE);
			fsFile.setLength(file.getSize());
			fsFile.setUid(user.getUid() + Utils.randomString(13));
		}

		List<FileBlock> fileBlocks = new LinkedList<>();
		try {
			flag = dataStorageService.saveFile(user, fsFile, file.getInputStream(), fileBlocks);
		} catch (IOException e) {
			return dto;
		}
		if (flag) {
			fileBlockRepository.save(fileBlocks);
			fsFile.setLastModified(DateTime.now().getMillis());
			fileRepository.save(fsFile);
			parentDir.setLastModified(DateTime.now().getMillis());
			fileRepository.save(parentDir);
			
			if (update) {
				updateHistory(fsFile);
			} else {
				createHistoryItem(fsFile);
			}
			
			dto.setStatusCode(update ? FSOperationDTO.STATUS_SUCCESS_UPDATE : FSOperationDTO.STATUS_SUCCESS);
			logger.info("Created/updated file: {}", fsFile.getPath());
		} else {
			logger.info("File: '{}' not created", fsFile.getPath());
			dto.setStatusCode(FSOperationDTO.STATUS_OPERATION_FAILED);
		}
		return dto;
	}

	private void createHistoryItem(FSFile fsFile) {
		FSFileHistoryItem historyItem = new FSFileHistoryItem();
		historyItem.setFileUid(fsFile.getUid());
		historyItem.setModifiedTime(fsFile.getLastModified());
		historyItemRepository.save(historyItem);
	}
	

	private void updateHistory(FSFile fsFile) {
		FSFileHistoryItem historyItem = new FSFileHistoryItem();
		historyItem.setFileUid(fsFile.getUid());
		historyItem.setModifiedTime(fsFile.getLastModified());
		historyItemRepository.save(historyItem);
	}
	

	private FSFile checkFileExist(FSOperationDTO dto, String parentUid, String fileName) {
		FSFile fsFile = fileRepository.findByParentUidAndName(parentUid, fileName);
		if (fsFile != null) {
			dto.setStatusCode(fsFile.getFileType() == FileType.DIRECTORY ?
					FSOperationDTO.STATUS_DIRECTORY_EXISTS :
					FSOperationDTO.STATUS_FILE_EXISTS);
			dto.putInfo("exist.name", fsFile.getName());
			return fsFile;
		}
		return null;
	}

	public void sendFile(User user, String fileUid, HttpServletResponse response) {
		FSFile fsFile = fileRepository.findByUid(fileUid);
		if (fsFile != null) {
			List<FileBlock> blocks = fileBlockRepository.findAllByFileUid(fsFile.getUid());
			try {
				response.setHeader("Content-disposition", "attachment; filename=" + fsFile.getName());
				dataStorageService.getFile(user, blocks, response.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public List<FSFileHistoryItem> getHistory(User user, String fileUid) {
		List<FSFileHistoryItem> historyItems = historyItemRepository.findAllByFileUid(fileUid);
		Collections.sort(historyItems, FSFileHistoryItem.TIME_DESC_COMPARATOR);
		return historyItems;
	}
}
