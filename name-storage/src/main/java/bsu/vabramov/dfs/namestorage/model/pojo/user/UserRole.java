package bsu.vabramov.dfs.namestorage.model.pojo.user;

/**
 * @author Vlad
 * @since 29.05.2016
 */
public enum  UserRole {
    ROLE_ADMIN, ROLE_USER
}
