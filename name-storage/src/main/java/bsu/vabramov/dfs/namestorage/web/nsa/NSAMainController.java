package bsu.vabramov.dfs.namestorage.web.nsa;

import bsu.vabramov.dfs.namestorage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Vlad
 * @since 29.05.2016
 */
@Controller
public class NSAMainController extends NSAdminControllerBase {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String greeting(Model model) {
        return "ns-admin-index";
    }

    @RequestMapping(value = "/new-user", method = RequestMethod.POST)
    @ResponseBody
    public Boolean newUser(@RequestParam String username, @RequestParam String password) {
        return userService.createNewUser(username, password) != null;
    }
}
