package bsu.vabramov.dfs.namestorage.model.pojo.file;

import java.util.Comparator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "files_history")
public class FSFileHistoryItem {
	@Id
    @GeneratedValue
    private Long id;
	private String fileUid;
	private long modifiedTime;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileUid() {
		return fileUid;
	}
	public void setFileUid(String fileUid) {
		this.fileUid = fileUid;
	}
	public long getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(long modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	
	public static final Comparator<FSFileHistoryItem> TIME_DESC_COMPARATOR = (it1, it2) -> {
		return (int) (it2.modifiedTime - it1.modifiedTime);
	};
}
