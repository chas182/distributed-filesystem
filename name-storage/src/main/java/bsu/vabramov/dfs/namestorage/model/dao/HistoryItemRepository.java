package bsu.vabramov.dfs.namestorage.model.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFileHistoryItem;

@Repository
public interface HistoryItemRepository extends CrudRepository<FSFileHistoryItem, Long> {

	List<FSFileHistoryItem> findAllByFileUid(String uid);
	
}
