package bsu.vabramov.dfs.namestorage.model.pojo.file;

import java.util.List;

/**
 * @author Vlad Abramov <vabramov@exadel.com>
 * @since 15.2.16
 */
public class FileTree {

    private String id;
    private String text;
    private String type = "dir";
    private List<FileTree> children;
//    private FileStatuses fileStatuses;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<FileTree> getChildren() {
        return children;
    }

    public void setChildren(List<FileTree> children) {
        this.children = children;
    }

//    public FileStatuses getFileStatuses() {
//        return fileStatuses;
//    }
//
//    public void setFileStatuses(FileStatuses fileStatuses) {
//        this.fileStatuses = fileStatuses;
//    }
}
