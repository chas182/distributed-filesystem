package bsu.vabramov.dfs.namestorage.model.pojo.user;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by vlad on 14.2.16.
 */
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String uid;
    @Column(unique = true)
    private String login;
    private String password;
    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                "role='" + userRole + '\'' +
                '}';
    }
}
