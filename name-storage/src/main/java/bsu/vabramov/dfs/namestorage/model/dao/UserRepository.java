package bsu.vabramov.dfs.namestorage.model.dao;

import bsu.vabramov.dfs.namestorage.model.pojo.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by vlad on 14.2.16.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    User findByLogin(String login);
}
