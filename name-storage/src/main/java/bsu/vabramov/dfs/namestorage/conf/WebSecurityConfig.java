package bsu.vabramov.dfs.namestorage.conf;

import bsu.vabramov.dfs.namestorage.util.AuthSuccessHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .exceptionHandling()
                .accessDeniedPage("/need-access")
                .and()
            .authorizeRequests()
                .antMatchers("/assets/**", "/webjars/**", "/ns-admin/new-user").permitAll()
                .antMatchers("/ns-admin**").hasRole("ADMIN")
                .antMatchers("/swa**").hasRole("USER")
                .antMatchers("/**").authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .successHandler(authenticationSuccessHandler())
                .permitAll()
                .and()
            .logout()
                .permitAll();
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler() {
        return new AuthSuccessHandler();
    }
}