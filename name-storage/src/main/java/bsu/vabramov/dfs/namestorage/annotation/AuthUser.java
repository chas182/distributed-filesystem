package bsu.vabramov.dfs.namestorage.annotation;

import java.lang.annotation.*;

/**
 * This annotation was made for retrieving registered users from Spring context.
 *
 * @author Vlad
 * @since 29.05.2016
 * @see java.security.Principal
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthUser {
}
