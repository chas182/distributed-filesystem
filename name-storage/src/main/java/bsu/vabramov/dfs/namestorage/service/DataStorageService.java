package bsu.vabramov.dfs.namestorage.service;

import bsu.vabramov.dfs.core.ds.DataStorageApi;
import bsu.vabramov.dfs.core.model.CreatedDirDTO;
import bsu.vabramov.dfs.core.settings.FSSettings;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFile;
import bsu.vabramov.dfs.namestorage.model.pojo.file.FileBlock;
import bsu.vabramov.dfs.namestorage.model.pojo.user.User;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import retrofit2.Call;

import javax.servlet.ServletOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vlad
 * @since 17.01.2016
 */
public class DataStorageService {

    private static final Logger logger = LoggerFactory.getLogger(DataStorageService.class);

    private Map<String, DataStorageApi> dataStorageServers = new LinkedHashMap<>();

    @Autowired
    private FSSettings fsSettings;
    @Autowired
    private Path tempDir;
    @Autowired
    private Gson gson;
    @Autowired
    private HttpClient httpClient;

    public Collection<DataStorageApi> getDataStorageServers() {
        return dataStorageServers.values();
    }

    public Map<String, DataStorageApi> getDataStorageServersMap() {
        return dataStorageServers;
    }

    public void addDataStorageServer(String baseUrl, DataStorageApi dataStorageApi) {
        if (dataStorageApi != null) {
            dataStorageServers.put(baseUrl, dataStorageApi);
        }
    }
    
    public boolean createDir(String userLogin, String dirPath) {
    	boolean flag = true;
        for (DataStorageApi api : dataStorageServers.values()) {
        	try {
				flag &= api.createDir(userLogin, dirPath).execute().body().created;
			} catch (IOException e) {
				flag = false;
			}
        }
        return flag;
    }

    public boolean saveFile(User user, FSFile file, InputStream stream, List<FileBlock> fileBlocks) {
        Integer blockSize = fsSettings.getFileBlockSize();
        byte[] buffer = new byte[blockSize];
        int index = 0, blockNumber = 0, bytesCount;
        List<Map.Entry<String, DataStorageApi>> apis = dataStorageServers.entrySet().stream().collect(Collectors.toList());
        boolean flag = true;
        try {
            while ((bytesCount = stream.read(buffer)) != -1) {
                if (index == apis.size())
                    index = 0;
                FileBlock block = new FileBlock(
                        file.getUid(),
                        file.getPath() + "/fb-" + blockNumber,
                        blockNumber++,
                        apis.get(index).getKey());
                Path tempBlockFilePath = null;
                // save
                try {
                    byte[] tmp = new byte[bytesCount];
                    System.arraycopy(buffer, 0, tmp, 0, bytesCount);
                    block.setBlockSize(bytesCount);

                    tempBlockFilePath = Files.createTempFile(
                            Files.createDirectories(
                                    Paths.get(tempDir.toString(), file.getOwnerName(), "fb-" + file.getUid())),
                            "" + block.getIndexNumber(), ".fbtemp");
                    File tempBlockFile = tempBlockFilePath.toFile();
                    FileOutputStream fos = new FileOutputStream(tempBlockFile);
                    IOUtils.write(tmp, fos);
                    IOUtils.closeQuietly(fos);

                    HttpPost httppost = new HttpPost(apis.get(index).getKey() + "/upload-file-block");
                    HttpEntity mpEntity = MultipartEntityBuilder.create()
                            .addBinaryBody("fb", tempBlockFile, ContentType.create("application/octet-stream"), "fb-" + blockNumber)
                            .addTextBody("fp", block.getBlockPath())
                            .addTextBody("ul", user.getLogin()).build();

                    httppost.setEntity(mpEntity);
                    logger.info("executing request " + httppost.getRequestLine());
                    HttpResponse response = httpClient.execute(httppost);
                    HttpEntity resEntity = response.getEntity();

                    logger.debug(response.getStatusLine().toString());
                    CreatedDirDTO dto = null;
                    if (resEntity != null) {
                        dto = gson.fromJson(EntityUtils.toString(resEntity), CreatedDirDTO.class);
                    }
                    if (resEntity != null) {
                        EntityUtils.consume(resEntity);
                    }

                    flag &= dto.created;
                } catch (Exception e) {
                    return false;
                } finally {
                    if (tempBlockFilePath != null) {
                        Files.delete(tempBlockFilePath);
                    }
                }
                fileBlocks.add(block);
                index++;
            }
            return flag;
        } catch (IOException e) {
            return false;
        }
    }

    public boolean deleteFileBlocks(User user, List<FileBlock> fileBlocks) {
        boolean flag = true;
        Map<String, Set<String>> resBlocks = fileBlocks.stream().map(fb ->  {
            Map<String, Set<String>> res = new HashMap<>();
            Set<String> blockPaths = new HashSet<>();
            blockPaths.add(fb.getBlockPath());
            fb.getDataStorageUlrs().forEach(url -> res.put(url, blockPaths));
            return res;
        }).reduce((m1, m2) -> {
            Map<String, Set<String>> res = new HashMap<>(m1);
            m2.entrySet().forEach(e -> {
                if (res.containsKey(e.getKey())) {
                    Set<String> blockPaths = res.get(e.getKey());
                    blockPaths.addAll(e.getValue());
                    res.put(e.getKey(),blockPaths);
                } else {
                    res.put(e.getKey(), e.getValue());
                }
            });
            return res;
        }).get();

        for (Map.Entry<String, Set<String>> entry : resBlocks.entrySet()) {
            Call<CreatedDirDTO> call = dataStorageServers.get(entry.getKey()).deleteFile(
                    user.getLogin(),
                    entry.getValue().stream().collect(Collectors.toList()));
            try {
                CreatedDirDTO dto = call.execute().body();
                flag &= dto.created;
            } catch (IOException e) {
                flag = false;
            }
        }
        return flag;
    }

    public void getFile(User user, List<FileBlock> blocks, ServletOutputStream outputStream) {
        Collections.sort(blocks, FileBlock.INDEX_NUMBER_COMPARATOR);

        for (FileBlock block : blocks) {
            try {
                URIBuilder builder = new URIBuilder(block.getDataStorageUlrs().get(0) + "/get-file-block");
                builder.setParameter("ul", user.getLogin())
                        .setParameter("bp", block.getBlockPath());

                HttpGet httpget = new HttpGet(builder.build());


                logger.info("executing request " + httpget.getRequestLine());
                HttpResponse response = httpClient.execute(httpget);
                HttpEntity resEntity = response.getEntity();

                logger.debug(response.getStatusLine().toString());
                IOUtils.copy(resEntity.getContent(), outputStream);

                EntityUtils.consume(resEntity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
