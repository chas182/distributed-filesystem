package bsu.vabramov.dfs.namestorage.model.pojo.file;

/**
 * @author Vlad
 * @since 06.06.2016
 */
public enum ChangeType {
    CREATE, UPDATE, DELETE
}
