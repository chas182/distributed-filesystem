package bsu.vabramov.dfs.namestorage.model.pojo.file;

/**
 * @author Vlad
 * @since 18.01.2016
 */
public class FileInfo {

//    "accessTime": 0,
//            "childrenNum": 24,
//            "fileId": 16455,
//            "storagePolicy": 0,
    private long blockSize;//: 0,
    private String owner;//": "simulator",
    private String group;//": "supergroup",
    private long length;//": 0,
    private long modificationTime;//": 1451472996190,
    private String pathSuffix;//": "amrld",
    private int permission;//": "777",
    private int replication;//": 0,
    private FileType type = FileType.DIRECTORY;//": "DIRECTORY"

    public long getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(long blockSize) {
        this.blockSize = blockSize;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getModificationTime() {
        return modificationTime;
    }

    public void setModificationTime(long modificationTime) {
        this.modificationTime = modificationTime;
    }

    public String getPathSuffix() {
        return pathSuffix;
    }

    public void setPathSuffix(String pathSuffix) {
        this.pathSuffix = pathSuffix;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    public int getReplication() {
        return replication;
    }

    public void setReplication(int replication) {
        this.replication = replication;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }
}
