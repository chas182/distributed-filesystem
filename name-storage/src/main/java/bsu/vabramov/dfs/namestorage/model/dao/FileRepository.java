package bsu.vabramov.dfs.namestorage.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bsu.vabramov.dfs.namestorage.model.pojo.file.FSFile;

/**
 * @author Vlad Abramov <vabramov@exadel.com>
 * @since 15.2.16
 */
@Repository
public interface FileRepository extends CrudRepository<FSFile, Long> {

    @Query("select f from FSFile f where f.uid like concat(:uid,'%') and f.path=:path")
    FSFile findByOwnerUidAndPath(@Param("uid") String uid, @Param("path") String path);
    FSFile findByUid(String uid);
    FSFile findByParentUidAndName(String parentUid, String name);

    List<FSFile> findAllByOwnerUid(String uid);
    List<FSFile> findAllByParentUid(String uid);

}
