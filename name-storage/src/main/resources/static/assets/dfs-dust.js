(function ($, dust, exports) {
    "use strict";

    Date.prototype.customFormat = function(formatString){
        var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhhh,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
        YY = ((YYYY=this.getFullYear())+"").slice(-2);
        MM = (M=this.getMonth()+1)<10?('0'+M):M;
        MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
        DD = (D=this.getDate())<10?('0'+D):D;
        DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][this.getDay()]).substring(0,3);
        th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
        formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);
        h=(hhh=this.getHours());
        if (h==0) h=24;
        //if (h>12) h-=12;
        hh = h<10?('0'+h):h;
        hhhh = h<10?('0'+hhh):hhh;
        AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
        mm=(m=this.getMinutes())<10?('0'+m):m;
        ss=(s=this.getSeconds())<10?('0'+s):s;
        return formatString.replace("#hhhh#",hhhh).replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
    };


    var filters = {
        'fmt_bytes': function (v) {
            var UNITS = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'ZB'];
            var prev = 0, i = 0;
            while (Math.floor(v) > 0 && i < UNITS.length) {
                prev = v;
                v /= 1024;
                i += 1;
            }

            if (i > 0 && i < UNITS.length) {
                v = prev;
                i -= 1;
            }
            return Math.round(v * 100) / 100 + ' ' + UNITS[i];
        },

        'fmt_percentage': function (v) {
            return Math.round(v * 100) / 100 + '%';
        },

        'fmt_time': function (v) {
            var s = Math.floor(v / 1000), h = Math.floor(s / 3600);
            s -= h * 3600;
            var m = Math.floor(s / 60);
            s -= m * 60;

            var res = s + " sec";
            if (m !== 0) {
                res = m + " mins, " + res;
            }

            if (h !== 0) {
                res = h + " hrs, " + res;
            }

            return res;
        },

        'date_tostring' : function (v) {
            return new Date(Number(v)).toLocaleString();
        },

        'helper_to_permission': function (v) {
            var symbols = [ '---', '--x', '-w-', '-wx', 'r--', 'r-x', 'rw-', 'rwx' ];
            var vInt = parseInt(v, 8);
            var sticky = (vInt & (1 << 9)) != 0;

            var res = "";
            for (var i = 0; i < 3; ++i) {
                res = symbols[(v % 10)] + res;
                v = Math.floor(v / 10);
            }

            if (sticky) {
                var otherExec = (vInt & 1) == 1;
                res = res.substr(0, res.length - 1) + (otherExec ? 't' : 'T');
            }

            return res;
        },

        'helper_to_directory' : function (v) {
            return v === 'DIRECTORY' ? 'd' : '-';
        },

        'helper_to_acl_bit': function (v) {
            return v ? '+' : "";
        },

        'fmt_number': function (v) {
            return v.toLocaleString();
        },

        'last_modified': function (v) {
            return new Date(v).customFormat('#DD#/#MM#/#YYYY# #hh#:#mm#')
        },

        'owner_login': function (v) {
            return v.login;
        }
    };
    $.extend(dust.filters, filters);

    /**
     * Load a sequence of JSON.
     *
     * beans is an array of tuples in the format of {url, name}.
     */
    function load_json(beans, success_cb, error_cb) {
        var data = {}, error = false, to_be_completed = beans.length;

        $.each(beans, function(idx, b) {
            if (error) {
                return false;
            }
            $.get(b.url, function (resp) {
                data[b.name] = resp;
                to_be_completed -= 1;
                if (to_be_completed === 0) {
                    success_cb(data);
                }
            }).error(function (jqxhr, text, err) {
                error = true;
                error_cb(b.url, jqxhr, text, err);
            });
        });
    }

    exports.load_json = load_json;

}($, dust, window));
