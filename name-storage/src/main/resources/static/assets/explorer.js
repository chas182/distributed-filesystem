
var statusCodes = {
	operationFailed: -1,
	success: 1,
	directoryExist: 2,
	fileExist: 3,
	successUpdate: 4
};

(function($) {

    "use strict";

    window.blockUI = function () {
        $.blockUI({
            message: '<img src="/assets/block.gif">',
            css: {border: 'none', backgroundColor: 'transparent'}
        });
    };

    window.unblockUI = function () {
        $.unblockUI();
    };

    window.showInfoPopup = function (what, options) {
        var optionsLoc = {title: undefined, description: undefined, delay: undefined, closeCallback: function() {}};
        copyProps(optionsLoc, options);
        var $body = $('body'),
            icon,
            title,
            timeout,
            style,
            description = optionsLoc.description || "";
        switch (what) {
            case 'success':
                title = optionsLoc.title || 'Saved';
                icon = '<img src="/assets/ok_icon.png">';
                style = 'background-color: #dff0d8; border-color: #d6e9c6;';
                timeout = optionsLoc.delay || 2000;
                break;
            case 'warning':
                title = optionsLoc.title ||'Warning';
                icon = '<img src="/assets/warning-icon.png">';
                timeout = optionsLoc.delay || 3000;
                style = 'background-color: #fcf8e3; border-color: #faebcc;';
                break;
            case 'error':
                title = optionsLoc.title ||'Error';
                icon = '<img src="/assets/error-icon.png">';
                timeout = optionsLoc.delay || 4000;
                style = 'background-color: #f2dede; border-color: #ebccd1;';
                break;
            default:
                return;
        }
        $body.append(
            '<div class="act-result" style="opacity: 0; z-index: 10000; box-shadow: 0 5px 15px rgba(0,0,0,.5);' + style + '">' +
            '<h1 class="act-result-title">' + title + '</h1>' +
            icon +
            '<p>' + description + '</p>' +
            '</div>');

        var $elem = $('.act-result').fadeTo('slow', 1);
        setTimeout(function () {
            $elem.fadeTo('slow', 0, function () {
                $elem.remove();
                optionsLoc.closeCallback();
            })
        }, timeout)
    };

    window.copyProps = function(to, from) {
        for (var key in from) {
            if (from.hasOwnProperty(key)) {
                to[key] = from[key];
            }
        }
    };
    
    var curDirId = "", rootDirId = "", jsTreeInst = null;

    function network_error_handler(url) {
        return function (jqxhr, text, err) {
            switch(jqxhr.status) {
                case 401:
                    var msg = 'Authentication failed when trying to open ' + url + ': Unauthorized.';
                    break;
                case 403:
                    if(jqxhr.responseJSON !== undefined && jqxhr.responseJSON.RemoteException !== undefined) {
                        var msg = '' + jqxhr.responseJSON.RemoteException.message + "";
                        break;
                    }
                    var msg = 'Permission denied when trying to open ' + url + ': ' + err + '';
                    break;
                case 404:
                    var msg = 'Path does not exist on HDFS or WebHDFS is disabled.  Please check your path or enable WebHDFS';
                    break;
                default:
                    var msg = 'Failed to retrieve data from ' + url + ': ' + err + '';
            }
            show_err_msg(msg);
        };
    }

    function show_err_msg(msg) {
        $('#alert-panel-body').html(msg);
        $('#alert-panel').show();
    }

    function append_path(prefix, s) {
        var l = prefix.length;
        var p = l > 0 && prefix[l - 1] == '/' ? prefix.substring(0, l - 1) : prefix;
        return p + '/' + s;
    }

    function get_response(data, type) {
        return data[type] !== undefined ? data[type] : null;
    }

    function get_response_err_msg(data) {
        return data.RemoteException !== undefined ? data.RemoteException.message : "";
    }

    function initFileTree(fileTree) {
        jsTreeInst = $('#files-tree')
            .on('activate_node.jstree', function (e, data) {
        		browseFolder(data.node.id);
                getProps(data.node.id)
            })
            .jstree({
                core: {
                    data: fileTree.children, // get root children
                    multiple: false
                },
                types : {
                    "#" : {
                        "valid_children" : ["dir", "file"]
                    },
                    "dir" : {
                        "icon" : "glyphicon glyphicon-folder-close",
                        "valid_children" : ["dir", "file"]
                    },
                    "file" : {
                        "icon" : "glyphicon glyphicon-file",
                        "valid_children" : []
                    }
                },
                "plugins" : ["state", "types", "wholerow"]
            });
    }

    function browseFolder(id) {
        curDirId = id;

        $.ajax({
            url: "/swa/fs?op=LS&du=" + id,
            method: "GET",
            cashed: false,
            beforeSend: function() {
                $('#panel').css("display", "none");
                $("#loading-table").css("display", "block");
            },
            success: function(files) {
                $('#panel').css("display", "block");
                $("#loading-table").css("display", "none");
                dust.render('explorer', files, function(err, out) {
                    $('#panel').html(out);

                    var DELAY = 500, clicks = 0, timer = null;
                    var $fileTableRows = $($("#file-table").find(".clickable-row"));
                    $fileTableRows.on("click", function(e){

                        var $this = $(this);
                        clicks++;  //count clicks
                        var fileType = $this.find("td[data-file-id]").data("file-type");
                        var fileActions = $('.file-action');
                        fileActions.css('display', 'none');
                        if(clicks === 1) {
                            $fileTableRows.removeClass("selected");
                            $(this).addClass("selected");
                            if (fileType == "FILE") {
                                fileActions.css('display', 'block');
                                $('#download-file').attr('href', '/swa/download/' + $this.find("td[data-file-id]").data("file-id"));
                                getProps($this.find("td[data-file-id]").data("file-id"));
                                clicks = 0;
                                return;
                            }
                            timer = setTimeout(function() {
                                getProps($this.find("td[data-file-id]").data("file-id"));
                                clicks = 0;
                            }, DELAY);
                        } else {
                            clearTimeout(timer);
                            var idToBrowse = $this.find("td[data-file-id]").data("file-id");
                            browseFolder(idToBrowse);
                            clicks = 0;
                        }
                    }).on("dblclick", function(e){
                            e.preventDefault();  //cancel system double-click event
                    });
                });
            }
        });
    }
    function getProps(id) {
        if (id == null) {
            $("#no-file-select").css("display", "block");
            $('#file-props-wrapper').css("display", "none");
            $("#loading-props").css("display", "none");
            return;
        }
        $("#no-file-select").css("display", "none");
        $.ajax({
            url: "/swa/fs?op=INF&du=" + id,
            method: "GET",
            cashed: false,
            beforeSend: function() {
                $('#file-props-wrapper').css("display", "none");
                $("#loading-props").css("display", "block");
            },
            success: function (file) {
                $("#loading-props").css("display", "none");
                $('#file-props-wrapper').css("display", "block");

                dust.render(file.fileType === "DIRECTORY" ? 'dir-props' : 'file-props', {file: file}, function(err, out) {
                    $('#file-props-panel').html(out);
                    $('#get-file-history').click(function(e){
                    	
                    });
                });
            }
        });
    }

    function init() {
        dust.loadSource(dust.compile($('#tmpl-explorer').html(), 'explorer'));
        dust.loadSource(dust.compile($('#tmpl-block-info').html(), 'block-info'));
        dust.loadSource(dust.compile($('#tmpl-dir-props').html(), 'dir-props'));
        dust.loadSource(dust.compile($('#tmpl-file-props').html(), 'file-props'));

        $("#home-dir").click(function(e) {
            browseFolder(rootDirId);
            getProps(null);
        });

        $("a[href=#file-tree-panel]").click(function(e) {
            var span = $($(this).find("span"));
            if (span.hasClass("glyphicon-triangle-right")) {
                span.removeClass("glyphicon-triangle-right");
                span.addClass("glyphicon-triangle-bottom");
            } else {
                span.removeClass("glyphicon-triangle-bottom");
                span.addClass("glyphicon-triangle-right");
            }
        });

        $.ajax({
            url: "/swa/files-info",
            method: "GET",
            success: function(fileTree) {
                initFileTree(fileTree);
                rootDirId = fileTree.id;
                browseFolder(fileTree.id);
                getProps(null)
            }
        });

        $('#create-dir-btn').click(function(e) {
            e.preventDefault();
            var newDir = $("#new-dir").val();
            $.ajax({
                url: "/swa/fs?op=CREATE_DIR&pid=" + curDirId + "&nd=" + newDir,
                method: 'PUT',
                cached: false,
                beforeSend: function() {
                    $('#mkdir-modal').modal('hide');
                    blockUI();
                },
                success: function(dto) {
                	unblockUI();
            		switch (dto.statusCode) {
					case statusCodes.directoryExist:
						showInfoPopup('warning', {description: "Directory with name '" + dto.info['exist.name'] + "' already exists."});
						break;
					case statusCodes.fileExist:
						showInfoPopup('warning', {description: "File with name '" + dto.info['exist.name'] + "' already exists."});
						break;
					case statusCodes.success:
						jsTreeInst.jstree("destroy");
	                    initFileTree(dto.fileTree);
	                    browseFolder(curDirId);
						break;
					}
                }
            })
        });

        $('#mkdir-modal').on('show.bs.modal', function (event) {
            $(this).find('input').val("")
        });

        $('#upload-file').click(function(e) {
            e.preventDefault();
            $('#inputFile').click();
        });
        
        function sendFile(e){
            var fileName = $(this).val();
            var data = new FormData();
            jQuery.each($("#inputFile")[0].files, function(i, file) {
                data.append('file', file);
            });
            data.append('pid', curDirId);
            if (fileName !== "")
                $.ajax({
                    url: "/swa/fs/upload", type: 'POST',
                    data: data,
                    processData: false, contentType: false, crossDomain: true, cashed: false,
                    beforeSend: function() {
                        blockUI();
                        document.getElementById("inputFile").value = "";
                    },
                    success: function(dto) {
                        unblockUI();
                        switch (dto.statusCode) {
    					case statusCodes.directoryExist:
    						showInfoPopup('warning', {description: "Directory with name '" + dto.info['exist.name'] + "' already exists."});
    						break;
    					case statusCodes.successUpdate:
    						showInfoPopup('success', {description: "File '" + dto.info['exist.name'] + "' was successfully updated."});
                            browseFolder(curDirId);
    						break;
    					case statusCodes.success:
    						showInfoPopup('success', {description: 'File successfully uploaded.'});
    	                    browseFolder(curDirId);
    						break;
    					}
                    },
                    error: function() {
                        showInfoPopup('error', {description: 'Произошла ошибка при отправке данных серверу.'});
                        unblockUI();
                    }
                });
        }
        
        $('#inputFile').change(sendFile);
    }

    init();

})(window.jQuery);


