#!/bin/bash

USAGE=$'Usage: dfs-help.sh <command> <args...>\n
Commands:
\tadd-user <username> <groupname>\t- add user with spesified username
\tremove-user <username>\t\t- remove user with username
\tadd-group <groupname>\t\t- add user group with specified name
\tremove-group <groupname>\t- remove user group with specified name'

# if no args specified, show usage
if [ $# -le 1 ]; then
  echo "No argumemts was specified."
  echo "$USAGE"
  exit 1
fi

case $1 in
   "add-user") echo '1586729AV' | sudo -S useradd -G $3 $2
   ;;
   "remove-user") echo '1586729AV' | sudo -S userdel $2
   ;;
   "add-group") echo '1586729AV' | sudo -S groupadd $2
   ;;
   "remove-group") echo '1586729AV' | sudo -S groupdel $2
   ;;
esac
