package bsu.vabramov.dfs.datastorage.service;

import bsu.vabramov.dfs.core.model.CreatedDirDTO;
import bsu.vabramov.dfs.core.settings.FSSettings;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

/**
 * Created by vlad on 18.1.16.
 */
@Service
@PropertySource("classpath:settings.properties")
public class DataStorageService {
    private static final Logger logger = LoggerFactory.getLogger(DataStorageService.class);

    @Autowired
    private FSSettings fsSettings;
    @Value("${data.storage.id}")
    private Integer dsId;

    private Path tempDir;

    @PostConstruct
	public void init() throws Exception {
		tempDir = Files.createTempDirectory("dstemp-" + UUID.randomUUID().toString());
	}
    
    public CreatedDirDTO createDir(String userLogin, String dirName) {
        FSSettings.DataStorage dataStorage = fsSettings.getDataStorage(dsId);
        CreatedDirDTO dto = new CreatedDirDTO();
        try {
            logger.info("Created dir: {}", 
            		Files.createDirectories(Paths.get(
            				dataStorage.getDataDir() + "/" + userLogin + dirName)).toString());
        } catch (IOException e) {
            logger.error("{}", e.getMessage());
            dto.setCreated(false);
        }
        dto.setCreated(true);
        return dto;
    }

	public CreatedDirDTO saveFileBlock(String userLogin, String filePath, MultipartFile fileBlock) {
		CreatedDirDTO dto = new CreatedDirDTO();
		try {
			FSSettings.DataStorage dataStorage = fsSettings.getDataStorage(dsId);
			Path file = Paths.get(dataStorage.getDataDir() + "/" + userLogin + filePath);
			Files.createDirectories(file.getParent());
			Files.createFile(file);
			FileOutputStream fos = new FileOutputStream(file.toFile());
			IOUtils.copy(fileBlock.getInputStream(), fos);
			IOUtils.closeQuietly(fos);
			dto.created = true;
		} catch (IOException e) {
			dto.created = false;
		}
		return dto;
	}

	public CreatedDirDTO deleteFileBlocks(String userLogin, List<String> blocksPaths) {
		CreatedDirDTO dto = new CreatedDirDTO();
		try {
			for (String blockPath : blocksPaths) {
				Files.delete(Paths.get(fsSettings.getDataStorage(dsId).getDataDir() + "/" + userLogin + blockPath));
			}
		} catch (IOException e) {
			dto.created = false;
		}
		return dto;
	}

	public void getFileBlock(String userLogin, String blockPath, ServletOutputStream stream) {
		try (FileInputStream fis = new FileInputStream(fsSettings.getDataStorage(dsId).getDataDir() + "/" + userLogin + blockPath)) {
			IOUtils.copy(fis, stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
