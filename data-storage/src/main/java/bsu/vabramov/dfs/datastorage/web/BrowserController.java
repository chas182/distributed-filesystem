package bsu.vabramov.dfs.datastorage.web;

import bsu.vabramov.dfs.core.model.CreatedDirDTO;
import bsu.vabramov.dfs.datastorage.service.DataStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by vlad on 18.1.16.
 */
@RestController
public class BrowserController {

    @Autowired
    private DataStorageService dataStorageService;

    @RequestMapping(value = "/create-dir", method = RequestMethod.PUT)
    public CreatedDirDTO createDir(@RequestParam String ul, @RequestParam String d) {
        return dataStorageService.createDir(ul, d);
    }
    
    @RequestMapping(value = "/upload-file-block", method = RequestMethod.POST)
    public CreatedDirDTO uploadFileBlock(@RequestParam("fb") MultipartFile fileBlock,
    									@RequestParam String fp,
    									@RequestParam String ul) {
    	return dataStorageService.saveFileBlock(ul, fp, fileBlock);
    }
    
    @RequestMapping(value = "/delete-file", method = RequestMethod.DELETE)
    public CreatedDirDTO deleteFile(@RequestParam("ul") String userLogin, @RequestParam("bp") List<String> blocksPaths) {
    	return dataStorageService.deleteFileBlocks(userLogin, blocksPaths);
    }

    @RequestMapping(value = "get-file-block", method = RequestMethod.GET)
    public void getFileBlocks(@RequestParam("ul") String userLogin, @RequestParam("bp") String blockPath, HttpServletResponse response) {
        try {
            dataStorageService.getFileBlock(userLogin, blockPath, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
